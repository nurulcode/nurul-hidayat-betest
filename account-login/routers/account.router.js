import { Router } from "express";
import { accountAuth, accountGetById, accountRegister, accountUpdate } from "../controllers/account.controller.js";
import { protect, tokenEmail } from "../middleware/auth.middleware.js";
const router = Router()

router.route('/')
.post(tokenEmail, accountRegister)
.put(protect, accountUpdate)

router.route('/login')
.post(accountAuth)

router.route('/profile')
.put(protect, accountUpdate)
.get(protect, accountGetById)

export default router;