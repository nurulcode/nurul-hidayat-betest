import app from "../index.js";
import request from "supertest";
import AccountLogin from "../models/account.model.js";

jest.useRealTimers();

describe("account login", () => {
    it("should return 200", async () => {
        const response = await request(app)
            .post("/api/accounts/login")
            .set("content-type", "application/json")
            .send({
                userName: "superboy",
                password: "secret121",
            });

        expect(response.status).toBe(200);
        expect(response.body.status).toBe(true);
    });

    it("should return 500", async () => {
        const response = await request(app)
            .post("/api/accounts/login")
            .set("content-type", "application/json")
            .send({
                userName: "superboy",
                password: "secret123",
            });

        expect(response.status).toBe(500);
        expect(response.body.status).toBe(false);
    });
});

describe("account regiter", () => {
    it("account registration successful", async () => {
        const token =
            "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDViZDUyYmVhNzlkNWY0YmJiNmZmMCIsImlhdCI6MTcxNTg0NjQ4MiwiZXhwIjoxNzE4NDM4NDgyfQ.DXDxjOxWGlFUnLULzD9eArPb7PpYMBRbv_spLvRiV1k";
        const response = await request(app)
            .post("/api/accounts?" + token)
            .set("content-type", "application/json")
            .send({
                userName: "superboy1",
                password: "secret123",
            });

        expect(response.status).toBe(201);
        expect(response.body.status).toBe(true);

        const TOKEN = response.body.token;
    });

    it("account failed account already exists", async () => {
        const token =
            "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDViZDUyYmVhNzlkNWY0YmJiNmZmMCIsImlhdCI6MTcxNTg0NjQ4MiwiZXhwIjoxNzE4NDM4NDgyfQ.DXDxjOxWGlFUnLULzD9eArPb7PpYMBRbv_spLvRiV1k";
        const response = await request(app)
            .post("/api/accounts?" + token)
            .set("content-type", "application/json")
            .send({
                userName: "superboy1",
                password: "secret123",
            });

        expect(response.status).toBe(400);
        expect(response.body.status).toBe(false);
    });
});

describe("account profile", () => {
    it("retrieve account data", async () => {
        const login = await request(app)
            .post("/api/accounts/login")
            .set("content-type", "application/json")
            .send({
                userName: "superboy1",
                password: "secret123",
            });

        const TOKEN = login.body.data.token;

        const response = await request(app)
            .get("/api/accounts/profile")
            .set("content-type", "application/json")
            .set("Authorization", `Bearer ${TOKEN}`);

        expect(response.status).toBe(200);
        expect(response.body.status).toBe(true);
    });

    it("update account data", async () => {
        const login = await request(app)
            .post("/api/accounts/login")
            .set("content-type", "application/json")
            .send({
                userName: "superboy1",
                password: "secret123",
            });

        const TOKEN = login.body.data.token;

        const response = await request(app)
            .put("/api/accounts/profile")
            .set("content-type", "application/json")
            .set("Authorization", `Bearer ${TOKEN}`)
            .send({
                password: "secret121",
            });

        expect(response.status).toBe(200);
        expect(response.body.status).toBe(true);
    });
});

describe("remove account", () => {
    it("remove account", async () => {
        await AccountLogin.deleteOne({ userName: "superboy1" });
    });
});
