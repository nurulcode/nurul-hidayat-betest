import express from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import cors from "cors";
import connectDB from "./config/db.js";
import accountRouter from "./routers/account.router.js";
dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded());
app.use(cors())

if (process.env.NODE_ENV == "development") {
    app.use(morgan("dev"));
}

connectDB()
const port = process.env.PORT || 3001;
function notFound(req, res, next) {
    res.status(404);
    res.json('404 - Not Found');
}
app.use('/api/accounts', accountRouter)


app.listen(port, () => {
    console.log(`Server is listening on port ${port}...`);
});

export default app
