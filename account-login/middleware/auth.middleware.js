import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { errorHandler } from "../utils/errorHandler.util.js";
import AccountLogin from "../models/account.model.js";
dotenv.config();

const protect = async (req, res, next) => {
    try {
        let token = req.headers.authorization;

        if (token && token.startsWith("Bearer")) {
            try {
                token = token.split(" ")[1];
                req.token = token;
                const secret = process.env.JWT_SECRET || "";
                const decoded = jwt.verify(token, secret);

                if (typeof decoded === "string") {
                    res.status(401);
                    errorHandler(res, 401, "Invalid token");
                }

                const id = decoded.id;
                const accountExist = await AccountLogin.findById(id).select(
                    "-password"
                );

                if (accountExist?.userName) {
                    req.account = accountExist;
                    next();
                }

                if (!token) {
                    errorHandler(res, 401, "Not authorized, no token");
                }
            } catch (error) {
                errorHandler(res, 401, "Not authorized, token failed");
            }
        }

        if (!token) {
            errorHandler(res, 401, "Not authorized, no token");
        }
    } catch (error) {
        errorHandler(res, 401, error.message);
    }
};

const tokenEmail = async (req, res, next) => {
    try {
        let token = req.query.token;
        if (token) {
            try {
                const secret = process.env.JWT_SECRET || "";
                const decoded = jwt.verify(token, secret);

                if (typeof decoded === "string") {
                    res.status(401);
                    errorHandler(res, 401, "Invalid token");
                }

                req.tokenEmail = decoded.id;
                next();

                if (!token) {
                    errorHandler(res, 401, "Not authorized, no token");
                }
            } catch (error) {
                errorHandler(res, 401, "Not authorized, token failed");
            }
        }

        if (!token) {
            errorHandler(res, 401, "Not authorized, no token");
        }
    } catch (error) {
        errorHandler(res, 401, error.message);
    }
};

const isAdmin = async (req, res, next) => {
    try {
        if (!req.account.isAdmin) {
            errorHandler(res, 401, "Not authorized as an admin");
        }
        next();
    } catch (error) {
        throw new Error(error.message);
    }
};

export { protect, isAdmin, tokenEmail };
