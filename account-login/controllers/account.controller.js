import { errorHandler, successHandler } from "../utils/errorHandler.util.js";
import AccountLogin from "../models/account.model.js";
import { body, param, validationResult } from "express-validator";
import {
    generateRefreshToken,
    generateToken,
} from "../utils/generateToken.util.js";
import mongoose from "mongoose";
import { insertService } from "../services/query.service.js";

const accountAuth = async (req, res) => {
    await body("userName").notEmpty().run(req);
    await body("password").notEmpty().run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty) {
        res.status(400).json({
            status: false,
            message: "validation failed",
            errors: errors.array(),
        });
    }

    const { userName, password, test } = req.body;

    try {
        const account = await AccountLogin.findOne({ userName });

        if (account && (await account.verifyPassword(password))) {
            account.lastLoginDateTime = Date();
            const update = await account.save();

            const token = generateToken(update._id, update.userId);

            insertService(
                {
                    accountId: update.id,
                    userId: update.userId,
                    lastLoginDateTime: update.lastLoginDateTime,
                    status: "ACCOUNT",
                },
                token
            );

            successHandler(res, 200, "Login account successfully", {
                accountId: update?.id,
                userName: update.userName,
                lastLoginDateTime: update.lastLoginDateTime,
                token: token,
                refreshToken: generateRefreshToken(update._id),
            });
        } else {
            errorHandler(res, 500, "account or password salah");
        }
    } catch (error) {
        // console.log(error.message);
        errorHandler(res, 500, "Invalid account data");
    }
};

const accountRegister = async (req, res) => {
    await body("userName").notEmpty().run(req);
    await body("password").notEmpty().isLength({ min: 6 }).run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            status: false,
            message: "Validation failed",
            errors: errors.array(),
        });
    }

    const { userName, password } = req.body;

    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const exist = await AccountLogin.findOne({ userName: userName });
        if (exist) {
            errorHandler(res, 400, "AccountLogin already exist!");
            return;
        }

        const userExist = await AccountLogin.findOne({ userId: req.tokenEmail });
        if (userExist) {
            errorHandler(res, 400, "AccountLogin already exist!");
            return;
        }

        const account = await AccountLogin.create({
            userName,
            password,
            userId: req.tokenEmail,
        });

        if (account) {
            await session.commitTransaction();
            session.endSession();

            successHandler(res, 201, "Register account successfully", {
                accountId: account.id,
                userName: account.accountname,
            });
        } else {
            await session.abortTransaction();
            session.endSession();
            errorHandler(res, 400, "Invalid account data");
        }
    } catch (error) {
        await session.abortTransaction();
        session.endSession();
        errorHandler(res, 500, "Internal Server Error");
    }
};

const accountGetById = async (req, res) => {
    try {
        if (!req.account) {
            errorHandler(404, "AccountLogin not found", res);
        }

        const account = await AccountLogin.findById(req.account.id).select(
            "-password"
        );
        if (account) {
            successHandler(res, 200, "Get data succesfully", {
                accountId: account?._id,
                userName: account?.userName,
                lastLoginDateTime: account?.lastLoginDateTime,
            });
        } else {
            errorHandler(res, 404, "Account not found");
        }
    } catch (error) {
        errorHandler(res, 500, "Internal Server Error");
    }
};

const accountUpdate = async (req, res) => {
    try {
        if (!req.account) {
            errorHandler(404, "AccountLogin not found", res);
        }

        const account = await AccountLogin.findById(req.account._id);

        if (!account) {
            errorHandler(404, "AccountLogin not found", res);
        } else {
            if (req.body?.password) {
                account.password = req.body?.password;
            }

            const update = await account.save();
            successHandler(res, 200, "Update data succesfully", {
                accountId: update?._id,
                userName: update?.userName,
                token: generateToken(update?._id, update.userId),
                refreshToken: generateRefreshToken(update._id, update.userId),
            });
        }
    } catch (error) {
        errorHandler(res, 500, "Internal Server Error");
    }
};

export { accountRegister, accountGetById, accountUpdate, accountAuth };
