import mongoose, { Schema } from "mongoose";
import bcrypt from "bcryptjs";

const accountSchema = new Schema({
    userName: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    lastLoginDateTime: {
        type: Date,
    },
    userId: {
        type: String,
        required: false,
        unique: true
    },
}, {
    timestamps: true
});

accountSchema.pre('save', async function (next) {
    const account = this

    if(!account.isModified('password')) return next()

    const salt = await bcrypt.genSalt(10)
    const hashed = await bcrypt.hash(account.password, salt)
    account.password = hashed;
    next();
})

accountSchema.methods = {
    verifyPassword: async function (password) {
        return bcrypt.compare(password, this.password)
    }
}

const AccountLogin = mongoose.model('AccountLogin', accountSchema);

export default AccountLogin;