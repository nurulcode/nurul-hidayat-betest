import jwt from "jsonwebtoken"
import dotenv from "dotenv";
dotenv.config();

const generateToken = (id, userId) => {
    const secretKey = process.env.JWT_SECRET ? process.env.JWT_SECRET : 'secretKey';
    return jwt.sign({ id, userId}, secretKey, {
        expiresIn: '30d'
    })
}

const generateRefreshToken = (id, userId) => {
    const secretKeyRefresh = process.env.JWT_SECRET_REFRESH ? process.env.JWT_SECRET_REFRESH : 'secretKeyRefresh';
    return jwt.sign({ id, userId }, secretKeyRefresh, {
        expiresIn: '30d'
    })
}

export {
    generateToken,
    generateRefreshToken
}