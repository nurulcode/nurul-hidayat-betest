import UserInfo from "../../user-info/models/user-info.model.js";
import QueryData from "../models/query.model.js";
import { errorHandler, successHandler } from "../utils/errorHandler.util.js";
import redisClient from '../config/redis.js'

const queryInsert = async (req, res) => {
    const {
        accountId,
        lastLoginDateTime,
        status,
        userId,
        fullName,
        accountNumber,
        emailAddress,
        registrationNumber,
    } = req.body;

    try {
        if (status == "ACCOUNT") {
            const account = await QueryData.findOne({ userId });
            if (account) {
                account.accountId = accountId;
                account.lastLoginDateTime = lastLoginDateTime;
                await account.save();
            }

            successHandler(res, 200, "success");
        }

        if (status == "USER INFO") {
            const account = await QueryData.findOne({ userId });
            if (!account) {
                await QueryData.create({
                    userId,
                    fullName,
                    accountNumber,
                    emailAddress,
                    registrationNumber,
                });
            } else {
                account.userId = userId;
                account.fullName = fullName;
                account.accountNumber = accountNumber;
                account.emailAddress = emailAddress;
                account.registrationNumber = registrationNumber;
                await account.save();
            }

            successHandler(res, 200, "success");
        }
    } catch (error) {
        console.log(error.message);
        errorHandler(res, 500, "Invalid account data");
    }
};

const queryGet = async (req, res) => {
    const { accountNumber, registrationNumber } = req.body;
    try {
        let users;
        if (accountNumber) {
            users = await QueryData.findOne({ accountNumber });
        }

        if (registrationNumber) {
            users = await QueryData.findOne({ registrationNumber });
        }

        if (users) {
            successHandler(res, 200, "Get data", {
                userId: users.userId,
                fullName: users.fullName,
                accountNumber: users.accountNumber,
                emailAddress: users.emailAddress,
                registrationNumber: users.registrationNumber,
            });
        } else {
            successHandler(res, 200, "Data not found", {});
        }
    } catch (error) {
        errorHandler(res, 500, "Invalid account data");
    }
};

const queryByLastLoginDateTime = async (req, res) => {
    try {
        const threeDaysAgo = new Date();
        threeDaysAgo.setDate(threeDaysAgo.getDate() - 3);

        const users = await UserInfo.find({
            lastLoginDateTime: { $gt: threeDaysAgo },
        });

        if (users) {
            successHandler(res, 200, "Get data", {
                userId: users.userId,
                fullName: users.fullName,
                accountNumber: users.accountNumber,
                emailAddress: users.emailAddress,
                registrationNumber: users.registrationNumber,
            });
        } else {
            successHandler(res, 200, "Data not found", {});
        }
    } catch (error) {
        errorHandler(res, 500, "Invalid account data");
    }
};

const queryGetAll = async (req, res) => {
    try {
        const cachedData = await redisClient.get('GetAll');
        
        if (!cachedData) {
            await new Promise(resolve => setTimeout(resolve, 5000)); 
            
            const users = await QueryData.find({});
            await redisClient.setEx('GetAll', 3600, JSON.stringify(users));
        }
    
        const value = await redisClient.get('GetAll');
        successHandler(res, 200, "Get data", JSON.parse(value));
    } catch (error) {
        console.log(error.message);
        throw new Error("Gagal mengambil info pengguna berdasarkan nomor akun");
    }
};

export { queryInsert, queryGet, queryByLastLoginDateTime, queryGetAll };
