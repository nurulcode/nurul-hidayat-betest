import express from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import cors from "cors";
import connectDB from "./config/db.js";
import queryRouter from "./routers/query.router.js";
dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded());
app.use(cors())

if (process.env.NODE_ENV == "development") {
    app.use(morgan("dev"));
}

connectDB()
const port = process.env.PORT || 3002;

app.use('/api/query', queryRouter)

app.listen(port, () => {
    console.log(`Server is listening on port ${port}...`);
});

export default app
