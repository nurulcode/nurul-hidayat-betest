import mongoose, { Schema } from "mongoose";

const querySchema = new Schema({
    accountId: {
        type: String,
        unique: true
    },
    userId: {
        type: String,
        unique: true
    },
    emailAddress: {
        type: String,
        unique: true
    },
    fullName: {
        type: String,
    },
    accountNumber: {
        type: Number,
        unique: true
    },
    lastLoginDateTime: {
        type: Date,
    },
    status: {
        type: String,
    },
}, {
    timestamps: true
});


const QueryData = mongoose.model('QueryData', querySchema);

export default QueryData;