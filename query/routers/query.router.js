import { Router } from "express";
import { protect } from "../middleware/auth.middleware.js";
import {
    queryByLastLoginDateTime,
    queryGet,
    queryGetAll,
    queryInsert,
} from "../controllers/query.controller.js";
const router = Router();

router.route("/insert").post(protect, queryInsert);
router.route("/get-user").post(protect, queryGet);
router.route("/by-last-login").post(protect, queryByLastLoginDateTime);
router.route("/get-all").get(protect, queryGetAll);

export default router;
