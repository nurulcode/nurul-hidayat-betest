import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { errorHandler } from "../utils/errorHandler.util.js";
dotenv.config();

const protect = async (req, res, next) => {
    try {
        let token = req.headers.authorization;
    
        console.log(token);
        if (token && token.startsWith("Bearer")) {
            try {
                token = token.split(" ")[1];
                const secret = process.env.JWT_SECRET || "";
                const decoded = jwt.verify(token, secret);

                if (typeof decoded === "string") {
                    res.status(401);
                    errorHandler(res, 401, "Invalid token");
                }
           
                if (!token) {
                    errorHandler(res, 401, 'Not authorized, no token')
                }

                next()
            } catch (error) {
                console.log(error);
                errorHandler(res, 401, "Not authorized, token failed");
            }
        }

        if (!token) {
            errorHandler(res, 401, "Not authorized, no token" );
        }
    } catch (error) {
        errorHandler(res, 401, error.message);
    }
};

export { protect };
