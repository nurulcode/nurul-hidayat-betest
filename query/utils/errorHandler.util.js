
const errorHandler = (res, statusCode, message, next) =>  {
    res.status(statusCode).json({
        status : false,
        message : message,
    })
}
const successHandler = (res,statusCode, message, data, next) =>  {
    res.status(statusCode).json({
        status : true,
        message : message,
        data: data
    })
}

export {
    errorHandler,
    successHandler
};