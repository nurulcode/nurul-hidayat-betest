
import { createClient } from 'redis';

const redisClient = await createClient({
    password: process.env.REDIS_PASS,
})
  .on('error', err => console.log('Redis Client Error', err))
  .connect();

export default redisClient