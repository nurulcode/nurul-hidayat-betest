 ## Menjalankan Aplikasi
 Untuk menjalankan aplikasi, Anda dapat menggunakan Makefile yang sudah disediakan. Jalankan perintah berikut:

Aplikasi menggunakan docker untuk  menjalankan redis, gunakan perintah

	make run
untuk menjalankan redis,lalu install semua lib dengan perintah

	npm install
	
## Mengakses Aplikasi

Setelah semua aplikasi berjalan, Anda dapat mengaksesnya menggunakan browser atau alat pengujian REST API seperti Postman. Berikut adalah URL untuk mengakses masing-masing bagian:

-   Account Login: [http://localhost:3001](http://localhost:3001/)
-   User Info: [http://localhost:3000](http://localhost:3000/)
-   Query: [http://localhost:3002](http://localhost:3002/)

## Fitur Aplikasi

User Info: [http://localhost:3000/api/user-info]
tidak menggunakan akses token
- Register [POST] URL/register

menggunakan akses token
- Get Data [GET] URL/get
- Update [PUT] URL/update

Account Login: [http://localhost:3001/api/accounts]
menggunakan akses token
- Register Account [POST]  URL/
- Update Account [PUT] URL/
- Login Account [POST]  URL/login
- Login Account [PUT]  URL/profile
- Login Account [GET]  URL/profile

Account Login: [http://localhost:3002/api/accounts]
menggunakan akses token
- Get User By accountNumber atau registrationNumber [POST]  URL/get-user"
- Get User 3 hari terakhir [PUT] URL/by-last-login
- Get All User menggunakan Redis [GET]  URL/get-all