import { insertService } from "../services/query.service.js";
import { UserService } from "../services/user-info.service.js";
import { errorHandler, successHandler } from "../utils/errorHandler.util.js";
import { generateToken } from "../utils/generateToken.util.js";
import { RegistrationNumberGenerator } from "../utils/registrationNumberGenerator.js";

export class UserController {
    static async createUser(req, res) {
        try {
            const accountNumberExist = await UserService.findOne({
                accountNumber: req.body.accountNumber,
            });
            if (accountNumberExist) {
                errorHandler(res, 401, "accountNumber sudah terfadtar");
            }

            const emailAddressExist = await UserService.findOne({
                emailAddress: req.body.emailAddress,
            });
            if (emailAddressExist) {
                errorHandler(res, 401, "accountNumber sudah terfadtar");
            }
            const user = await UserService.createUser({
                fullName: req.body.fullName,
                accountNumber: req.body.accountNumber,
                emailAddress: req.body.emailAddress,
                registrationNumber: await RegistrationNumberGenerator.generate(
                    req.body.accountNumber
                ),
            });

            const token = generateToken(user._id);

            insertService(
                {
                    userId: user._id,
                    fullName: user.fullName,
                    accountNumber: user.accountNumber,
                    emailAddress: user.emailAddress,
                    registrationNumber: user.registrationNumber,
                    status: "USER INFO",
                },
                token
            );

            successHandler(res, 201, "insert data success", {
                userId: user._id,
                fullName: user.fullName,
                accountNumber: user.accountNumber,
                emailAddress: user.emailAddress,
                registrationNumber: user.registrationNumber,
                token: token,
            });
        } catch (error) {
            errorHandler(res, 500, error.message);
        }
    }

    static async updateUser(req, res) {
        try {
            const userExist = await UserService.findOne({
                _id: req.userId,
            });

            if (!userExist) {
                errorHandler(res, 401, "accountNumber belum terfadtar");
            }

            userExist.fullName = req.body.fullName;
            userExist.save();

            insertService(
                {
                    userId: userExist._id,
                    fullName: userExist.fullName,
                    accountNumber: userExist.accountNumber,
                    emailAddress: userExist.emailAddress,
                    registrationNumber: userExist.registrationNumber,
                    status: "USER INFO",
                },
                req.token
            );

            successHandler(res, 201, "update data success", {
                userId: userExist._id,
                fullName: userExist.fullName,
                accountNumber: userExist.accountNumber,
                emailAddress: userExist.emailAddress,
                registrationNumber: userExist.registrationNumber,
            });
        } catch (error) {
            errorHandler(res, 500, error.message);
        }
    }

    static async getUser(req, res) {
        try {
            const userExist = await UserService.findOne({
                _id: req.userId,
            });

            if (!userExist) {
                errorHandler(res, 401, "accountNumber belum terfadtar");
            }

            successHandler(res, 201, "get data success", {
                userId: userExist._id,
                fullName: userExist.fullName,
                accountNumber: userExist.accountNumber,
                emailAddress: userExist.emailAddress,
                registrationNumber: userExist.registrationNumber,
            });
        } catch (error) {
            errorHandler(res, 500, error.message);
        }
    }
}
