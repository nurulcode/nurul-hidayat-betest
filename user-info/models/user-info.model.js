import mongoose, { Schema } from "mongoose";

const userInfoSchema = new Schema(
    {
        fullName: {
            type: String,
            required: true,
        },
        accountNumber: {
            type: String,
            required: true,
            unique: true,
        },
        emailAddress: {
            type: String,
            required: true,
            unique: true,
        },
        registrationNumber: {
            type: String,
            required: true,
            unique: true,
        },
    },
    {
        timestamps: true,
    }
);

const UserInfo = mongoose.model("UserInfo", userInfoSchema);

export default UserInfo;
