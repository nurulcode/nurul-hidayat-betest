import { Router } from "express";
import { protect } from "../middleware/auth.middleware.js";
import { UserController } from "../controllers/user-info.controller.js";
const router = Router();

router.route("/register").post(UserController.createUser);
router.route("/get").put(protect, UserController.updateUser);
router.route("/update").put(protect, UserController.updateUser);

export default router;
