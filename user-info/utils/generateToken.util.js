import jwt from "jsonwebtoken"
import dotenv from "dotenv";
dotenv.config();

const generateToken = (id) => {
    const secretKey = process.env.JWT_SECRET ? process.env.JWT_SECRET : 'secretKey';
    return jwt.sign({ id }, secretKey, {
        expiresIn: '30d'
    })
}

const generateRefreshToken = (id) => {
    const secretKeyRefresh = process.env.JWT_SECRET_REFRESH ? process.env.JWT_SECRET_REFRESH : 'secretKeyRefresh';
    return jwt.sign({ id }, secretKeyRefresh, {
        expiresIn: '30d'
    })
}

export {
    generateToken,
    generateRefreshToken
}