import UserInfo from "../models/user-info.model.js";
import { UserService } from "../services/user-info.service.js";

export class RegistrationNumberGenerator {
    static async generate(number) {
        const date = new Date();
        const year = date.getFullYear().toString();
        const month = (date.getMonth() + 1).toString().padStart(2, "0");
        const day = date.getDate().toString().padStart(2, "0");
        const key = `RN:${year}${month}${day}`;

        const count = await UserInfo.find({}, '_id registrationNumber',).sort({ createdAt: -1 });

        let num = 1;
        if (count) {
            num = parseInt(count[0].registrationNumber.slice(-3), 10) + 1;
        }

        const sequence = num.toString().padStart(3, "0");
        return `${year}${month}${day}${sequence}`;
    }
}
