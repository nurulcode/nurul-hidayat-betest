import UserInfo from "../models/user-info.model.js";

export class UserRepository {
  static async createUser(data) {
    return await UserInfo.create(data);
  }

  static async getUserById(id) {
    return await UserInfo.findById({ id });
  }
  
  static async findOne(key) {
    return await UserInfo.findOne(key);
  }
  
  static async getAllUsers() {
    return await UserInfo.find();
  }
}

