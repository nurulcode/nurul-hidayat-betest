import { UserRepository } from "../repositories/user-info.repository.js";

export class UserService {
    static async createUser(data) {
        return await UserRepository.createUser(data);
    }

    static async getUserById(id) {
        return await UserRepository.getUserById(id);
    }

    static async findOne(key) {
        console.log(key);
        return await UserRepository.findOne(key);
    }
    
    static async getAllUsers() {
        return await UserRepository.getAllUsers();
    }
}
