import axios from "axios";

const targetBackendURL = "http://localhost:3002/api/query/insert"; 

const insertService = (data, token) => {
    const axiosConfig = {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    };

    axios
        .post(targetBackendURL, data, axiosConfig) 
        .then((response) => {
            console.log(response.data);
        })
        .catch((error) => {
            // console.error("Error saat mengirim data:", error);
        });
};

export {
    insertService
}