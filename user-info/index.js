import express from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import connectDB from "./config/db.js";
import userInfoRoute from "./routes/user-info.route.js";

dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded());
if (process.env.NODE_ENV == "development") {
    app.use(morgan("dev"));
}

connectDB()

app.use('/api/user-info', userInfoRoute)

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server is listening on port ${port}...`);
});
